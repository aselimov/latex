---
title: "<++>"
abstract: ""
author: Alex Selimov


###
### Bibliography settings
###
link-citations: true


###
### Formatting settings
###
documentclass: article
fontsize: 12pt
figPrefix: figure
#geometry: margin=1.0in
geometry: "left=3cm,right=3cm,top=2cm,bottom=2cm"
header-includes:
    - \usepackage{mathpazo}
    - \usepackage{amsmath}
urlcolor: blue

---


\newpage
# References
