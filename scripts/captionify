#!/bin/bash

##This script adds a caption to a figure using imagemagick
####
#Help
####
Help(){
    echo "Add a caption to an image using imagemagic"
    echo
    echo "Syntax: captionify [-h|-d|-o <out_file>|-s <size>|-p <pad>] infile message"
    echo "options:"
    echo "-h: Print this help message"
    echo '-d: Open image defined in $VIEWER'
    echo "-o: Specify output file, otherwise the output gets written to caption_infile"
    echo "-s: Specify font size in pixels"
    echo "infile: The file to be processed"
    echo "message: The message to be added"
}

##Image Viewer

args=()
argnum=0
outfile=""
display=0
pad=20

if [ "$VIEWER" = '' ]; then
    openin=sxiv
else
    openin=$VIEWER
fi

while [ $# -gt 0 ]; do 
    unset OPTIND
    unset OPTARG
    #Parse Flags
    while getopts 'hdo:s:p:' options; do
        case $options in 
            h)
                Help
                break;;
            d)
                display=1;;
            o)
                outfile="$OPTARG";;
            s)
                size="$OPTARG";;
            p)
                pad="$OPTARG";;
        esac
    done
    shift $((OPTIND-1))
    args+=("$1")
    (( argnum++ ))
    shift
done

if [ "$args" = "" ]; then 
    Help
elif [ $argnum -gt 2 ]; then 
    echo "Captionify takes only two positional parameter, please view help"
elif [ $argnum -lt 2 ]; then 
    echo "Captionify requires two positional parameters, please view help"
else
    infile="${args[0]}"
    message="${args[1]}"
    #Set defaults if variable unset
    if [ "$outfile" = "" ]; then 
        outfile="cap_$infile"  
    fi
    
    if [ "$size" = "" ]; then
        ih=$(identify -format %h $infile)
        echo "Height is $ih"
        size=$(echo "scale=0; $ih*10/100" | bc )
        echo "Size is $size"
    fi

    #Now add some small padding
    if [ $pad -gt 0 ]; then
        width=$(identify -format '%w' $infile)
        height=$(identify -format '%h' $infile)
        (( height = height + pad ))
        convert -extent "${width}x${height}" $infile $outfile
        infile=$outfile
    fi
    #Now run the actual imagemagick command
    convert -gravity center $infile \( -background '#FFF' -fill black -font Helvetica  -size x$size label:"$message" \)\
            -append $outfile
 

    if [ $display -eq 1 ]; then
        $openin $outfile 
    fi
fi

exit 1
