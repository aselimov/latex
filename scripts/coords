#!/bin/bash

##This creates a red to blue divergent colorbar with specified label and bounds.
####
#Help
####
Help(){
    echo "This creates a coordinate axis image file with specified x and y labels"
    echo
    echo "Syntax: colorbar [-h|-o outfile|-x width|-y height] xlabel ylabel"
    echo "options:"
    echo "-h: print this help"
    echo "-o: Specify file to write to"
    echo "-x: Specify width of image. Cannot be used with -y"
    echo "-y: Specify height of mage. Cannot be used with -x"
    echo "xlabel: Label for x axis"
    echo "ylabel: Label for y axis"
}


outfile='coords.png'
declare -a args
argnum=0
x=0
y=0
while [ $# -gt 0 ]; do
    unset OPTIND
    unset OPTARG
    #Parse Flags
    while getopts ':ho:x:y:' options; do 
        case $options in
            h)
                Help
                break;;
            o)
                outfile="$OPTARG";;
            x)
                x="$OPTARG"
                if [ $y -ne 0 ]; then
                    echo "Cannot use both -x and -y options"
                    break
                fi
                ;;
            y)
                y="$OPTARG"
                if [ $x -ne 0 ]; then
                    echo "Cannot use both -x and -y options"
                    break
                fi
                ;;
        esac
    done
    shift $((OPTIND-1))
    args+=("$1")
    (( argnum++ ))
    shift
 done

#Now pull out the required arguments
if [ "$args" = "" ]; then
    Help
elif [ $argnum -ne 2 ]; then 
    echo "coords requires three positional arguments, you have supplied $argnum. Please view help"
    echo ${args[0]} ${args[1]}
else
    xlabel="${args[0]}"
    ylabel="${args[1]}"

    #First copy colorbar file to current directory
    cp ~/typesetting/figure_templates/coords.png $outfile

    #Now create lower and upper labels
    convert -background " #FFF " -fill "#ff0000" -font Helvetica -size x180 label:"$xlabel" tmp.png
    convert +append -gravity south $outfile tmp.png $outfile
    convert -background " #FFF " -fill "#00ff00" -font Helvetica -size x180 label:"$ylabel" tmp.png
    convert -append -gravity west tmp.png $outfile $outfile

    #Final step is resizing
    if [ $x -gt 0 ]; then
        convert -scale "$x"x $outfile $outfile
    fi
    if [ $y -gt 0 ]; then
        convert -scale x"$y" $outfile $outfile
    fi
    rm tmp.png
    
fi
