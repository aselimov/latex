---
title: "<++>"


###
### Bibliography settings
###
bibliography:
    - /home/aselimov/typesetting/references.bib
csl: /home/aselimov/typesetting/ieee.csl
link-citations: true


###
### Formatting settings
###
documentclass: article
fontsize: 12pt
#geometry: margin=1.0in
geometry: "left=3cm,right=3cm,top=2cm,bottom=2cm"
header-includes:
    - \usepackage{times}
    - \usepackage{amsmath}
urlcolor: blue
output: 
    rmarkdown::pdf_document:
---

<++>

